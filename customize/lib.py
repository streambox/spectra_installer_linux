import logging
import os
import pathlib
import re
import shutil
import subprocess

import requests
import tqdm


def sync(src: str, dst: str, dry_run=False):
    cmd = [
        "aws",
        "s3",
        "sync",
        src,
        dst,
        "--grants",
        "read=uri=http://acs.amazonaws.com/groups/global/AllUsers",
        "--region",
        "us-west-2",
        "--no-progress",
    ]

    if dry_run:
        cmd.append("--dryrun")

    proc = subprocess.Popen(
        cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )

    logging.debug(cmd)
    outs, errs = proc.communicate(timeout=60)
    print(outs)


# Print every file with its size recursing through dirs
def recurse_dir(root_dir):
    root_dir = os.path.abspath(root_dir)
    for item in os.listdir(root_dir):
        item_full_path = os.path.join(root_dir, item)

        if os.path.isdir(item_full_path):
            recurse_dir(item_full_path)
        else:
            print("%s - %s bytes" % (item_full_path, os.stat(item_full_path).st_size))


def make_archive(source, destination):
    base = os.path.basename(destination)
    name = base.split(".")[0]
    format = base.split(".")[1]
    archive_from = os.path.dirname(source)
    archive_to = os.path.basename(source.strip(os.sep))
    print(source, destination, archive_from, archive_to)
    shutil.make_archive(name, format, archive_from, archive_to)
    shutil.move("%s.%s" % (name, format), destination)


def fetch(u1, target_path: pathlib.Path = None):
    logging.debug(f"requests.get({u1.url})")
    response = requests.get(u1.url, stream=True, allow_redirects=True)

    with open(str(target_path), "wb") as handle:
        for data in tqdm.tqdm(response.iter_content()):
            handle.write(data)


def match(line):
    if (
        re.search(r"quickstartguide-url", line)
        or re.search(r"spectra-zip-url", line)
        or re.search(r"spectra-ver-url", line)
    ):
        return True
    return False


def append_multiple_lines(file_name, lines_to_append):
    # Open the file in append & read mode ('a+')
    with open(file_name, "a+") as file_object:
        appendEOL = False
        # Move read cursor to the start of file.
        file_object.seek(0)
        # Check if file is not empty
        data = file_object.read(100)
        if len(data) > 0:
            appendEOL = True
        # Iterate over each string in the list
        for line in lines_to_append:
            # If file is not empty then append '\n' before first line for
            # other lines always append '\n' before appending line
            if appendEOL == True:
                file_object.write("\n")
            else:
                appendEOL = True
            # Append element at the end of file
            file_object.write(line)

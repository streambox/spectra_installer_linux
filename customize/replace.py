import customize.lib

file_name = "sample4.ini"


with open(file_name, "r") as file:
    lines = file.readlines()

with open(file_name, "w") as file:
    for line in lines:
        if not customize.lib.match(line):
            file.write(line)

list_of_lines = [
    "quickstartguide-url = https://streambox-spectra.s3-us-west-2.amazonaws.com/latest/linux/spectra_quickstart_linux.pdf",
    "spectra-zip-url = https://streambox-spectra.s3-us-west-2.amazonaws.com/latest/linux/spectra.zip",
    "spectra-ver-url = https://streambox-spectra.s3-us-west-2.amazonaws.com/latest/linux/version.txt",
]

customize.lib.append_multiple_lines(file_name, list_of_lines)

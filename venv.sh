#!/bin/bash

deactivate 2>/dev/null || true
python3 -mvenv .venv
source .venv/bin/activate
pip install --quiet --upgrade pip --requirement requirements.txt

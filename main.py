import argparse
import configparser
import logging
import pathlib
import shutil

import monacelli_pylog_prefs.logger

import customize.lib

parser = argparse.ArgumentParser()

parser.add_argument("--update-latest", action="store_true", default=False)
parser.add_argument("--sync-dryrun", action="store_true", default=False)
args = parser.parse_args()


monacelli_pylog_prefs.logger.setup(
    filename=f"{pathlib.Path(__file__).stem}.log", stream_level=logging.DEBUG
)

work_path = pathlib.Path("work")
work_path.mkdir(parents=True, exist_ok=True)

extract_to_path = pathlib.Path("spectra")
extract_to_path.mkdir(parents=True, exist_ok=True)

ini_path = pathlib.Path(".bumpversion.cfg")
ini = configparser.ConfigParser()
ini.read(ini_path)
spectra_version = ini["bumpversion"]["current_version"]

stage40_path = work_path / "stage40" / "linux" / spectra_version
stage40_path.mkdir(parents=True, exist_ok=True)
spectra_zip_path = stage40_path / "spectra.zip"

# Create this guide
# https://streambox-spectra.s3-us-west-2.amazonaws.com/latest/linux/spectra_for_linux_plus_resolve.pdf
doc_src_path = pathlib.Path("guides/Streambox_Spectra_for_Linux(AWS-Resolve)_122321.pdf")
doc_dst_path = stage40_path / "spectra_for_linux_plus_resolve.pdf"
shutil.copy(doc_src_path, doc_dst_path)

customize.lib.make_archive(
    str(extract_to_path.resolve()), str(spectra_zip_path.resolve())
)

changelog_src_path = "changelog.yml"
changelog_dst_path = stage40_path / "changelog.txt"
shutil.copy(changelog_src_path, changelog_dst_path)

version_txt_path = stage40_path / "version.txt"
version_txt_path.write_text(f"{spectra_version}")

customize.lib.recurse_dir(str(work_path))

s3_endpoint_versioned = f"s3://streambox-spectra/linux/{spectra_version}"
s3_endpoint_latest = f"s3://streambox-spectra/latest/linux"

customize.lib.sync(
    src=str(stage40_path.resolve()), dst=s3_endpoint_versioned, dry_run=args.sync_dryrun
)

# make this version a production version
customize.lib.sync(
    src=str(stage40_path.resolve()),
    dst=s3_endpoint_latest,
    dry_run=not args.update_latest,
)

if args.update_latest:
    logging.warning(f"you have overwrote production with this version")
